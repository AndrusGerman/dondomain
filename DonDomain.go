package dondomain

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	"gopkg.in/resty.v1"
)

// CheckDomain Verificar si el dominio es valido
func (ctx *DonDomainClient) CheckDomain(domain string) ([]CheckDomainModelDomain, error) {
	bt, err := ctx.CheckDomainBytes(domain)
	if err != nil {
		return []CheckDomainModelDomain{}, err
	}
	var output CheckDomainModel
	err = json.Unmarshal(bt, &output)
	if err != nil {
		return []CheckDomainModelDomain{}, fmt.Errorf("ErrorUnmarshal: %v", err)
	}
	return output.ResponseData.Domains, err
}

// CheckDomain Verificar si el dominio es valido
func (ctx *DonDomainClient) CheckDomainBytes(domain string) ([]byte, error) {
	if len(domain) <= 4 {
		return []byte{}, errors.New("Dominio muy corto, para ser validado")
	}
	apiURL := "https://simple-api.dondominio.net:443/domain/check/"
	var data = map[string]string{"apiuser": ctx.APIUser, "apipasswd": ctx.APIPassword, "domain": domain}
	cl := resty.New()
	r, err := cl.R().SetFormData(data).Post(apiURL)
	if err != nil {
		return []byte{}, fmt.Errorf("ErrorPost: %v", err)
	}
	return r.Body(), nil
}

// BuyDomain Comprar un dominio nuevo
func (ctx *DonDomainClient) BuyDomain(domain string, ip string, nameserver string, ownerID string) (BuyDomainModel, error) {
	apiURL := "https://simple-api.dondominio.net:443/domain/create/"
	data := map[string]string{
		"apiuser": ctx.APIUser, "apipasswd": ctx.APIPassword, "IP": ip, "domain": domain, "nameservers": nameserver,
		"ownerContactID": ownerID,
	}
	cl := resty.New()
	r, err := cl.R().SetFormData(data).Post(apiURL)
	var output BuyDomainModel
	if err != nil {
		return output, err
	}
	json.Unmarshal(r.Body(), &output)
	return output, nil
}

type BuyDomainModel struct {
	Success      bool     `json:"success"`
	ErrorCode    int      `json:"errorCode"`
	ErrorCodeMsg string   `json:"errorCodeMsg"`
	Action       string   `json:"action"`
	Version      string   `json:"version"`
	Messages     []string `json:"messages"`
}

// CheckAPIDomain Verificar la disponibilidad de la api
func (ctx *DonDomainClient) CheckAPIDomain() {
	apiURL := "https://simple-api.dondominio.net:443/tool/hello/"
	data := map[string]string{"apiuser": ctx.APIUser, "apipasswd": ctx.APIPassword}
	cl := resty.New()
	r, err := cl.R().SetFormData(data).Post(apiURL)
	if err != nil {
		fmt.Printf("The HTTP request failed with error %s\n", err)
	} else {
		fmt.Println(r.String())
	}
}

// GetInfoNameServer get info server
func (ctx *DonDomainClient) GetInfoNameServer(domain string) ([]GetInfoNameServerServers, error) {
	apiURL := "https://simple-api.dondominio.net:443/domain/getinfo/"
	var data = map[string]string{"apiuser": ctx.APIUser, "apipasswd": ctx.APIPassword, "domain": domain, "infoType": "nameservers"}
	cl := resty.New()
	r, err := cl.R().SetFormData(data).Post(apiURL)
	var output GetInfoNameServer
	json.Unmarshal(r.Body(), &output)
	return output.ResponseData.Nameservers, err
}

// DonDomainClient is model for new DonDomain
type DonDomainClient struct {
	APIUser     string
	APIPassword string
}

// ContainsSomeDNS verification
func (ctx *DonDomainClient) ContainsSomeDNS(domain string, dnss ...string) (bool, error) {
	b, er := ctx.ContainsThisDNS(domain, dnss...)
	if b {
		return b, nil
	}
	if er.Error() == "Algunas dns conciden" {
		return true, nil
	}
	return b, er
}

// ContainsThisDNS verification
func (ctx *DonDomainClient) ContainsThisDNS(domain string, dnss ...string) (bool, error) {
	if !strings.Contains(domain, ".") {
		return false, errors.New("This route is not domain")
	}
	infod, err := ctx.GetInfoNameServer(domain)
	if err != nil {
		return false, err
	}
	// Response date
	if len(infod) != 0 {
		var dnsOK = 0
		for _, val := range infod {
			for _, dns := range dnss {
				if val.Name == dns {
					dnsOK++
				}
			}
		}
		// DNS encontrados respuesta
		if dnsOK == 0 {
			return false, errors.New("Ningun dns concide")
		} else if dnsOK == len(dnss) {
			return true, nil
		} else {
			return false, errors.New("Algunas dns conciden")
		}
	}

	// Error no case
	return false, errors.New("Response is empyte")
}

// NewDonDomainClient create new donDomain client
func NewDonDomainClient(User string, Password string) *DonDomainClient {
	return &DonDomainClient{APIUser: User, APIPassword: Password}
}

type CheckDomainModel struct {
	Success      bool   `json:"success"`
	ErrorCode    int    `json:"errorCode"`
	ErrorCodeMsg string `json:"errorCodeMsg"`
	Action       string `json:"action"`
	Version      string `json:"version"`
	ResponseData struct {
		Domains []CheckDomainModelDomain `json:"domains"`
	} `json:"responseData"`
}

type CheckDomainModelDomain struct {
	Name      string  `json:"name"`
	Punycode  string  `json:"punycode"`
	Tld       string  `json:"tld"`
	Available bool    `json:"available"`
	Premium   bool    `json:"premium"`
	Price     float64 `json:"price"`
	Currency  string  `json:"currency"`
}

type GetInfoNameServer struct {
	Success      bool   `json:"success"`
	ErrorCode    int    `json:"errorCode"`
	ErrorCodeMsg string `json:"errorCodeMsg"`
	Action       string `json:"action"`
	Version      string `json:"version"`
	ResponseData struct {
		Name        string                     `json:"name"`
		Status      string                     `json:"status"`
		Tld         string                     `json:"tld"`
		DomainID    int                        `json:"domainID"`
		TsExpir     string                     `json:"tsExpir"`
		DefaultNS   bool                       `json:"defaultNS"`
		Nameservers []GetInfoNameServerServers `json:"nameservers"`
	} `json:"responseData"`
}

type GetInfoNameServerServers struct {
	Order int    `json:"order"`
	Name  string `json:"name"`
	Ipv4  string `json:"ipv4"`
}
